# Docker Swarm Setup Guide

This guide outlines the steps needed to set up a Docker Swarm environment for a MySQL database cluster.

## Step 1: Create a Docker Overlay Network

Run the following command on the Docker Swarm manager node to create an overlay network named `swarmnet`:

```shell
docker network create --driver overlay swarmnet
```

## Step 2: Label the Nodes

```shell
docker node update --label-add mydb1=true node01
docker node update --label-add mydb2=true node02
docker node update --label-add mydb3=true node03
```

## Step 3: Create Storage Volumes

```shell
docker volume create dbdata
docker volume create dblog
```
## Step 4: MySQL Configuration Files on each node 

mydb1.cnf (for node01):
```shell
nano mydb1.cnf 
```
```shell
[mysqld]
server-id=1
binlog-format=ROW
log-bin=mysql-bin
binlog-ignore-db=mysql
binlog-ignore-db=information_schema
binlog-ignore-db=performance_schema
binlog-ignore-db=sys
binlog-do-db=mydb
```

mydb2.cnf (for node02):
```shell
nano mydb2.cnf 
```
```shell
[mysqld]
server-id=2
log-bin=mysql-bin
relay-log=mysql-relay-bin
relay-log-index=mysql-relay-bin.index
log-slave-updates=1
read-only=1
replicate-ignore-db=mysql
replicate-ignore-db=information_schema
replicate-ignore-db=performance_schema
replicate-ignore-db=sys
replicate-do-db=mydb
```
mydb3.cnf (for node03):

```shell
nano mydb3.cnf 
```
```shell
[mysqld]
server-id=3
log-bin=mysql-bin
relay-log=mysql-relay-bin
relay-log-index=mysql-relay-bin.index
log-slave-updates=1
read-only=1
replicate-ignore-db=mysql
replicate-ignore-db=information_schema
replicate-ignore-db=performance_schema
replicate-ignore-db=sys
replicate-do-db=mydb
```

## Step 4 : Deploy Containers and Services 
```shell
docker stack deploy -c docker-compose.yml db
```

## Step 5 : Setup MySQL instances

Create a “replica” user on the Primary instance mydb1

SSH inside the Docker container on node01 

```shell
docker ps -a
```

get the container ID from the output , then ssh in the container 

```shell 
docker exec -it 2b9de7807a6e /bin/bash
```

from inside the container login as mysql root 

```shell
mysql -u root -p 
```

```shell
CREATE USER 'repli'@'%' IDENTIFIED WITH mysql_native_password BY 'SECURE_REPLI_PASSWORD';
GRANT REPLICATION SLAVE ON *.* TO 'repli'@'%';
```

create database 

```shell
CREATE DATABASE mydb;
```
Get the replication checkpoint from the Primary instance mydb1

SHOW MASTER STATUS;


The output should look something like below. We’ll need the file name and position to setup mydb2 and mydb3:

File: mysql-bin.000003
Position: 573
Binlog_Do_DB: mydb
Binglog_IgnoreDB: mysql,information_schema,performance_schema,sys

## step 6: Create the database on the Replica instances

```shell
CREATE DATABASE mydb;
```

## Step 7 : Setup Replication on both nodes 

```shell
STOP REPLICA;
CHANGE REPLICATION SOURCE TO
SOURCE_HOST = 'mydb1',
SOURCE_USER = 'repli',
SOURCE_PASSWORD = 'SECURE_REPLI_PASSWORD',
SOURCE_LOG_FILE = 'mysql-bin.000003',
SOURCE_LOG_POS = 573; 
```

```shell
START REPLICA;
```


